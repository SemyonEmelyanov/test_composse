package com.example.test

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat

internal object Permission {
    private var REQUEST_ID_MULTIPLE_PERMISSIONS = 7
    private var arrayPermission = mutableListOf<String>()
    fun checkPermission(context: Context){
        if (context.checkSelfPermission(Manifest.permission.CAMERA)
            == PackageManager.PERMISSION_DENIED)  arrayPermission.add(Manifest.permission.CAMERA)
        if (context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            == PackageManager.PERMISSION_DENIED)  arrayPermission.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if (context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            == PackageManager.PERMISSION_DENIED)  arrayPermission.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        if (arrayPermission.isNotEmpty()){
            ActivityCompat.requestPermissions(
                context as Activity, arrayPermission.toTypedArray(), REQUEST_ID_MULTIPLE_PERMISSIONS)
        }

    }
}